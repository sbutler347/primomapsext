		var imgLoadState = []; //for storing state of each row of results.
		function thumbnailPop(row,img){
			if (imgLoadState[row]!=1){ //if no image successfully loaded yet _in this row_, pop the image to the top.
				imgLoadState[row] = 1;
				var p = img.parentNode;
				p.className = "popThumbnailToTop "+p.className;
			}

		}
		function thumbnailHide(img){
			var p = img.parentNode;
			p.style.display = 'none';
		}

		var thumbnailQueries = [];
		function addThumbnailQueryUrl(url){
			thumbnailQueries[thumbnailQueries.length] = url;
		}

		function runThumbnailQueries(){
			for (i = 0; i<thumbnailQueries.length;i++){
			    var scriptElement = document.createElement("script");
			    var url = thumbnailQueries[i];
				l('thumbnail query: '+url);
   				scriptElement.setAttribute("src",url);
			    scriptElement.setAttribute("type", "text/javascript");
   				// make the request to Google Book Search
			    document.documentElement.firstChild.appendChild(scriptElement);
			}
		}

		function getGBSCover(covers){
			for (i in covers) {
				var cover = covers[i];
				if (cover){
					return cover.thumbnail_url;
				}
			}
			return undefined;
		}

		function displayDiv(divName){
			elem = document.getElementById(divName);
			if (elem.style.display=="block"){
				elem.style.display="none"
			}else{
				elem.style.display="block"
			}
			return false;
		}
