
var statusObj = null;

function isUpdateAllowed(overrideIsUpdateAllowed){
	if(overrideIsUpdateAllowed){
	 return true;
	}
	//this func should determine whether or not to run an ajax rtaUpdate.
	//if for some reason we can't find the checkbox or we run into any other problems,
	//we will go ahead w/ the ajax rtaUpdate, why? because 95% of the time it's useful to the user.
	try{
		var checkbox = document.getElementById("allowRefresh_chkbx");
		if (!checkbox.checked){ //we only don't update if we found the checkbox and it is unchecked.
			return false;
		}
	}catch(e){
	}
	//in any other case, do the update.
	return true;
}

function updateRTA(overrideIsUpdateAllowed){
	if (isUpdateAllowed(overrideIsUpdateAllowed)){
		var queryStatusURL = "rta.do";
		ajaxGet(queryStatusURL, handleResponse);
	}
}

function log(msg){
	try{
		console.debug("rta: "+msg);
	}catch(e){
		//do nothing
	}
}

function updateFullRTA(){
	if (isUpdateAllowed()){
		queryStatusURL = "rta.do?fn=full";
		ajaxGet(queryStatusURL, handleResponse);
	}
}
function updateEshelfRTA(){
	if (isUpdateAllowed()){
		queryStatusURL = "rta.do?fn=full";
		ajaxGet(queryStatusURL, handleResponse);
	}
}
function updateEshelfAvailabilityLinkRTA(overrideIsUpdateAllowed){
	if (isUpdateAllowed(overrideIsUpdateAllowed)){
		var queryStatusURL = "rta.do";
		ajaxGet(queryStatusURL, handleEshelfAvailabilityLinkResponse);
	}
}

/*****
AJAX code
*****/

var request = null;

function sleep(milliseconds) {
	  var start = new Date().getTime();
	  for (var i = 0; i < 1e7; i++) {
	    if ((new Date().getTime() - start) > milliseconds){
	      break;
	    }
	  }
	}



function handleResponse(){
	try{
		 
	if(checkReadyState(request)) //check to see if we have data.
	{
		
			//blink every now and again to tell us we're alive.
			//flicker(document.getElementById("flickerer"),true);
			//flicker(document.getElementById("blinkerer"));
			
			//here's the trick. we receive a statusObj serialized via JSON. the eval turns it back into an object.
			eval(request.responseXML.getElementsByTagName("json")[0].childNodes[0].nodeValue);
			
			
			//console.log("response:"+request.responseText);
			if (statusObj.length > 0){
				for (var i = 0; i<statusObj.length; i++){
				try{
					document.getElementById('RTASpan_' + i).innerHTML = statusObj[i][2];
					document.getElementById('RTADivTitle_' + i).innerHTML = statusObj[i][0];
					document.getElementById('RTADivTitle_' + i).className = statusObj[i][1];
					//do only for brief
					if(document.getElementById('availability_inlineDiv_' + i)){
						document.getElementById('availability_inlineDiv_' + i).innerHTML = request.responseXML.getElementsByTagName("availhtml")[i].childNodes[0].nodeValue;
					}
				
					var refDiv = document.getElementById('refresh' + i);
					if (refDiv != null) {
						refDiv.style.display='block';
					      
					}
					}catch(e){

					}
				}

			}

			//alert("test");
			//document.getElementById('RTASpan_0').innerHTML = "test";

	}else{//error?
		//alert("else error");
	}
	}catch(e){
		log(e.message);
	}
}

//function handleFull(){
//	if(checkReadyState(request)) //check to see if we have data.
//	{
//			//console.log("response:"+request.responseText);
//			document.getElementById('availabilityTile').innerHTML = request.responseText;
//			//alert("test");
//			updateRTA();
//			//document.getElementById('RTASpan_0').innerHTML = "test";
//
//	}else{//error?
//		//alert("else error");
//	}
//}
//
//function handleEshelf(){
//	if(checkReadyState(request)) //check to see if we have data.
//	{
////			console.log("response:"+request.responseText);
//			frames['demoLib'].document.getElementById('availabilityTile').innerHTML = request.responseText;
//			//alert("test");
//			updateEshelfAvailabilityLinkRTA();
//			//document.getElementById('RTASpan_0').innerHTML = "test";
//
//	}else{//error?
//		//alert("else error");
//	}
//}

//function handleEshelfAvailabilityLinkResponse(){
//	try{
//	if(checkReadyState(request)) //check to see if we have data.
//	{
//		eval(request.responseXML.getElementsByTagName("json")[0].childNodes[0].nodeValue);
//			if (statusObj.length > 0){
//				for (var i = 0; i<statusObj.length; i++){
//				try{
//					frames['demoLib'].document.getElementById('RTASpan_' + i).innerHTML = statusObj[i][2];
//					frames['demoLib'].document.getElementById('RTADivTitle_' + i).innerHTML = statusObj[i][0];
//					frames['demoLib'].document.getElementById('RTAAvailability_' + i).className = statusObj[i][1];
//					}catch(e){
//
//					}
//				}
//
//			}
//	}else{//error?
//		//alert("else error");
//	}
//	}catch(e){
//		log(e.message);
//	}
//}

