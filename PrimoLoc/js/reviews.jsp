<%@ include file="/views/taglibsIncludeAll.jspf" %>
<script>
function cancel()
{
	if(confirm("<fmt:message key='default.reviews.cancelReviewConfirm'/>"))
	{		
			document.getElementById('review_form').style.display='none';
			document.getElementById('writeRev').style.display='block';		
	}
}

function show_more(){
}

function edit_save()
{
	if(document.getElementById('agree04').checked)
	{	
		document.getElementById('review_form').style.display='none';
		document.getElementById('writeRev').style.display='block';
		var reviewForm = document.getElementById("displayForm");
      	reviewForm.submit();
	}
	else alert("<fmt:message key='default.reviews.agreeError'/>");
}

	

function remove(reviewId)
{
	if(confirm("<fmt:message key='default.reviews.cancelReviewConfirm'/>"))
	{
		document.getElementById('writeRev').style.display='block';
		//document.review.agree01.checked = false;		
		//document.review.agree02.checked = false;		
		var reviewForm = document.getElementById("displayForm");
		reviewForm.elements.reviewId.value 	= reviewId;
		reviewForm.elements.executeMode.value = 2;	
		reviewForm.submit();
	}
}

function new_review()
{
		document.getElementById('writeRev').style.display='none';
		document.getElementById('review_form').style.display='block';
		document.getElementById("displayForm").elements.executeMode.value = 0;		
}

function prepare_edit(rateReview,displayReviewWriter,reviewId)
{
	document.getElementById('review_form').style.display='block';
	var reviewForm = document.getElementById("displayForm");
	reviewForm.elements.review.value 				= document.getElementById("full_review_text_"+reviewId).innerHTML;
	reviewForm.elements.rateReview.value 			= rateReview;
	reviewForm.elements.displayReviewWriter.checked	= displayReviewWriter=='true';
	reviewForm.elements.reviewId.value 				= reviewId;
	reviewForm.elements.executeMode.value 			= 1;	
}

function showMore(extValueId){
	document.getElementById('ext_val_'+extValueId).style.display='none';
	document.getElementById('ext_val_more_'+extValueId).style.display='block';	
}
</script>
