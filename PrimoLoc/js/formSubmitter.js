function applyIE6WorkArounds(){
  try{
	//alert("applying fix");
	//'hail mary' fix that might help other forms in korean.
	/*var forms = document.getElementsByTagName('form');
	for(var i=0;i<forms.length;i++){
		forms[i].onsubmit = function(){
			manualFormSubmit(this.id);
		}
	}*/
	//run through all links in the page, apply Fucheng's fix.
	var links = document.getElementsByTagName('a');
	  for (var i=0;i<links.length;i++){
	      links[i].oldOnClick = links[i].onclick; //save any old onclicks
	      links[i].onclick = function(){
	              this.href = encodeURI(decodeURI(this.href)).replace(/%25/g,"%"); //Fucheng fixa
	              if (this.oldOnClick){ //if in fact our old onclick exists.
	                      this.oldOnClick(); //run it!
	              }
	      }
	  }
  }catch(er1){alert(er1);}



}
//add the function to be run onload.
addLoadEvent(function(){
	//alert('applying fix');
	applyIE6WorkArounds();
});

function manualFormSubmit(id){
	try{
	//alert('implementing fix');
		var form = null;
		if (!id){ //this handles situations where no id was provided.
			form = document.getElementsByName(this.name)[0];//if we throw an exception it means there are serious problems with form and we'll exit in the catch.
		}else{
			form = document.getElementById(id);
		}
		if (form){
			var inputs = form.getElementsByTagName('input');
			var selects = form.getElementsByTagName('select');
			var textareas = form.getElementsByTagName('textarea');
			var queryString;

			queryString = handleInputs(inputs, form);
			queryString += handleSelects(selects, form);
			queryString += handleTextareas(textareas, form);


			var completeQuery = form.action + queryString;

			if (form.name == 'searchForm'){ //specifying go_jx ensures the server returns JSON responseObject.url so we can handle the redirect ourselves.
				ajaxGet( completeQuery.replace('fn=go&','fn=go_jx&'), ajaxProcessSearchRedirect);
			}else{ //when we aren't in the searchForm, and we won't be using 'fn=go' (no redirect) then we just redirect the browser.
				window.location = completeQuery;
			}
		}
	}catch(err){
		alert(err);
	}
}

function handleTextareas(elems,form){
	var qstr = "";
	for(var i = 0; i < elems.length; i++){
		qstr += handleText(elems[i],form);
	}
	return qstr;
}

//every form element (except select) ends up going through this function. so this is where encoding happens.
function handleText (element,form){
	if (form && form.name == 'searchForm')//double-encoding when we need to protect against server-redirect in the case of searchForm.
		return element.name + '=' + encodeURI(encodeURI(element.value)) + '&';
	else
		return element.name + '=' + encodeURI(element.value) + '&';
}

//encoding here is purely defensive coding, all the values of select.options should be predefined english-only values.
function handleSelects(elems,form){
	var qstr = "";
	for(var i = 0; i < elems.length; i++){
		qstr += elems[i].name + '=' + encodeURI(elems[i].options[elems[i].selectedIndex].value) + '&';
	}
	return qstr;
}

function handleInputs(elems,form){
	var qstr = "";
	for(var i = 0; i < elems.length; i++){
		switch (elems[i].type){
			case 'text':
			case 'hidden':
				qstr += handleText(elems[i],form);
				break;
			case 'radio':
				qstr += handleRadios(elems[i],form);
				break;
			case 'checkbox':
				qstr += handleChecked(elems[i],form);
				break;
		}
	}
	return qstr;
}

function handleChecked(element,form){
	if (element.checked){
		return handleText(element,form);
	}
}

function handleRadios(elements,form){
	for (var i = 0;i < elements.length; i++){
		handleChecked(elements[i],form);
	}
}

//this processes the special case of searchForm where we need to perform the redirect manually via JavaScript instead of via HTTP-redirect because of the browser-deficiency.
function ajaxProcessSearchRedirect(){
		if(checkReadyState(request))
		{
			//alert(request.responseXML);
			try{
				eval(request.responseText);
				if(requestObj){
					window.location = requestObj.url.replace(/\%25/g,'%'); //apply the final step of Fucheng's fix here before redirect.
				}
			}catch(err){
				alert("Search Request Failed!")
			}
		}
}
