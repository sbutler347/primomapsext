/* init.js begin */
// add and remove from e-Shelf

function toeShelf(whom, classz) {
	if(classz=="toeShelf")
	{
		whom.className = "offeShelf";
		whom.title = "Remove from eShelf";
	}
	else
	{
		whom.className = "toeShelf";
		whom.title = "Add to eShelf";
	}
}

// show more or less refinement items
function more(what) {
document.getElementById('less_'+what).style.display = 'none';
document.getElementById('more_'+what).style.display = 'block';
document.getElementById('first_'+what).style.marginBottom = '0';
}
function less(what) {
document.getElementById('more_'+what).style.display = 'none';
document.getElementById('less_'+what).style.display = 'block';
document.getElementById('first_'+what).style.marginBottom = '20px';
}

// initialise page
var elems = document.getElementsByTagName("*");
for (var i = 0; i < elems.length; i++)
{
if (elems[i].className == 'offstage') elems[i].style.display = 'none';
}



function browserDetect(text) {
   str = (navigator.userAgent.toLowerCase()).indexOf(text) + 1;
   data = text;
   return str;
}

function saveLi()
{
	document.getElementById('saveLi').style.display = 'none';
	document.getElementById('selectTd').style.display = 'inline';

	document.getElementById('saveLi_bottom').style.display = 'none';
	document.getElementById('selectTd_bottom').style.display = 'inline';
}

function newSave()
{
document.getElementById('noJS_form').style.display = 'none';
document.getElementById('JS_form').style.display = 'block';
}

function ULevent(obj,linkx)
{
	if(linkx.className == "plus") // open
	{
		linkx.className = "minus";
		document.getElementById(obj).style.display = "block";
	}
	else // close
	{
		linkx.className = "plus";
		document.getElementById(obj).style.display = "none";
	}
}

function toggle(place)
{
	for(i=place;i<(place+5); i++)
		document.getElementById("checkbox0"+i).checked = false;
}

function restartx(flag)
{
	if(flag) document.getElementById("chs02").checked = true;
	else  document.getElementById("chs04").checked = true;
}

function changeSelectColor(obj)
{
	if(obj.value>0) obj.className = "white";
	else obj.className = "blue";
}

function tagsVerify(obj)
{
	if(obj.value=='2')
	{
		document.getElementById('tagTooltip_link').style.visibility = "visible";
		document.getElementById('tagTooltip_link').style.width = "21px";
	}
	else
	{
		document.getElementById('tagTooltip_link').style.visibility = "hidden";
		document.getElementById('tagTooltip_link').style.width = "1px";
	}
}

function localOrRemote()
{
	if(document.getElementById("lookIn").value==3) window.location = "results_layout_tabbed_remote.asp";
	else window.location = "results_layout_tabbed_local.asp";
}

function openSaveQuery(href)
{
	SaveQuery = openWindow(href,'query','width = 550, height = 330, resizable=1,scrollbars=1 ')

}

function Tags_Cloud_list(option, which)
{
	if(which==1)
	{
		if(option)
		{
			document.getElementById("myTags").style.display = "block";
			document.getElementById("myCloudDiv").style.display = "none";
			//document.getElementById("my_linkSpan").innerHTML ="<a href='#' onclick='Tags_Cloud_list(false, 1); return false;'>Cloud</a> / List";
		}
		else
		{
			document.getElementById("myTags").style.display = "none";
			document.getElementById("myCloudDiv").style.display = "block";
			//document.getElementById("my_linkSpan").innerHTML = "Cloud / <a href='#' onclick='Tags_Cloud_list(true, 1); return false;'>List</a>";
		}
	}
	else
	{
		if(option)
		{
			document.getElementById("everyCloudDiv").style.display = "none";
			document.getElementById("everyListDiv").style.display = "block";
			//document.getElementById("every_linkSpan").innerHTML = "<a href='#' onclick='Tags_Cloud_list(false, 2); return false;'>Cloud</a> / List";
		}
		else
		{
			document.getElementById("everyCloudDiv").style.display = "block";
			document.getElementById("everyListDiv").style.display = "none";
			//("every_linkSpan").innerHTML = "Cloud / <a href='#' onclick='Tags_Cloud_list(true, 2); return false;'>List</a>";
		}
	}

}

function clickEshelf(objId, chk)
{
	var addToEshelf= document.getElementById("addToEshelf_for_js"+(objId.split('addto_eshelf')[1])).innerHTML;
	var inEshelf= document.getElementById("inEshelf_for_js"+(objId.split('addto_eshelf')[1])).innerHTML;

	var labelID = 'label_eshelf' + (objId.split('addto_eshelf')[1]);
	if(chk) document.getElementById(labelID).innerHTML = inEshelf;
	else document.getElementById(labelID).innerHTML = addToEshelf;
}

function updateSelectEshelf(chk)
{

	var pushToElement = document.getElementById('pushTo');
	var optionIndex;
	//finding the index of the option of add/remove eshelf

	if(pushToElement!=null){
		for(var i=0; i<=pushToElement.options.length; i++){

			if(pushToElement.options[i].value=='create'){
			optionIndex=i;
			break;
			}
			if(pushToElement.options[i].value=='remove'){
			optionIndex=i;
			break;
			}
		}

		var addToEshelf= document.getElementById("addToEshelf_for_js").innerHTML;
		var removeFromEshelf= document.getElementById("removeFromEshelf_for_js").innerHTML;

		//updating the add to eshlef/remove from eshelf drop down in the page (in commands_tile)
		if(chk){
			document.getElementById('pushTo').options[optionIndex].innerHTML=removeFromEshelf;
			document.getElementById('pushTo').options[optionIndex].value='remove';
		}else{
			document.getElementById('pushTo').options[optionIndex].innerHTML=addToEshelf;
			document.getElementById('pushTo').options[optionIndex].value='create';
		}
	}
}



/* tooltip */

function show(linx, object)
{
    if (document.getElementById)
	{

		if((browserDetect('opera'))||(browserDetect('firefox')))
		{
			leftx = (findPosX(linx))-275;
			topx = (findPosY(linx))+15;

		}
		else if(browserDetect('msie'))
		{
			leftx = (findPosX(linx))+6;
			topx = (findPosY(linx))-3;
		}
		else
		{
			leftx = (findPosX(linx))-275;
			topx = (findPosY(linx))+15;
		}

        document.getElementById(object).style.left = leftx+'px';
        document.getElementById(object).style.top = topx+'px';
        document.getElementById(object).style.visibility = 'visible';
	}
}

function hide(obj)
{
	document.getElementById(obj).style.visibility = 'hidden';

}

function findPosX(obj)
{
	var curleft = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft;
}

function findPosY(obj)
{
	var curtop = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
	return curtop;
}

function browserDetect(text) {
   str = (navigator.userAgent.toLowerCase()).indexOf(text) + 1;
   data = text;
   return str;
}

if(document.getElementById('tagsTooltip')) document.getElementById('tagsTooltip').className = "tagsTooltip2";

function commandSave(href)
{
	if(href !='0') openWindow(href,'query','width = 650, height = 600, resizable=1,scrollbars=1 ');
}

function toggle_inlineAvailability()
{
	if(document.getElementById('availability_inlineDiv').style.display == 'none')
		document.getElementById('availability_inlineDiv').style.display = "";
	else document.getElementById('availability_inlineDiv').style.display = "none";
}

var bubbleAvail_timer;
function toggle_bubbleAvailability(linx, avail_objDiv)
{
	if(bubbleAvail_timer) clearTimeout(bubbleAvail_timer);
	document.getElementById(avail_objDiv).style.display = "";

	if(parseInt(findPosX(linx))<document.getElementById(avail_objDiv).scrollWidth)
		document.getElementById(avail_objDiv).style.width = findPosX(linx) + 60 +"px";

	leftx = findPosX(linx)-(parseInt(document.getElementById(avail_objDiv).scrollWidth)-100);
	topx = (findPosY(linx))+20;
	document.getElementById(avail_objDiv).style.left = leftx+'px';
	document.getElementById(avail_objDiv).style.top = topx+'px';
	document.getElementById(avail_objDiv).style.visibility = "visible";
}/*
function toggle_bubbleAvailability(linx, avail_objDiv)
{
	if(bubbleAvail_timer) clearTimeout(bubbleAvail_timer);
	document.getElementById(avail_objDiv).style.display = "";
	if((browserDetect('opera'))||(browserDetect('firefox')))
	{
		leftx = (findPosX(linx))-400;
		topx = (findPosY(linx))+20;
	}
	else if(browserDetect('msie'))
	{
		leftx = (findPosX(linx))-400;
		topx = (findPosY(linx))+0;
	}
	else
	{
		leftx = (findPosX(linx))-400;
		topx = (findPosY(linx))+20;
	}

	if(leftx<0) leftx = 0;
	document.getElementById(avail_objDiv).style.left = leftx+'px';
	document.getElementById(avail_objDiv).style.top = topx+'px';
	document.getElementById(avail_objDiv).style.visibility = 'visible';
}*/

function keepShow(avail_objDiv)
{
	if(bubbleAvail_timer) clearTimeout(bubbleAvail_timer);
	document.getElementById(avail_objDiv).style.visibility = 'visible';
}

function delayedHide(avail_objDiv)
{
	bubbleAvail_timer = setTimeout("hide('"+avail_objDiv+"')", 1000);
}

// reviews stars
/*
var reviewsStar01 = new Image; reviewsStar01.src = "../images/reviews_1stars_link.gif";
var reviewsStar02 = new Image; reviewsStar02.src = "../images/reviews_2stars_link.gif";
var reviewsStar03 = new Image; reviewsStar03.src = "../images/reviews_3stars_link.gif";
var reviewsStar04 = new Image; reviewsStar04.src = "../images/reviews_4stars_link.gif";
var reviewsStar05 = new Image; reviewsStar05.src = "../images/reviews_5stars_link.gif";

var reviews_imgsArr = new Array('', reviewsStar01, reviewsStar02, reviewsStar03, reviewsStar04, reviewsStar05);

function reviewStar(num, img)
{
	document.getElementById(img).src = reviews_imgsArr[num].src;
}


*/
