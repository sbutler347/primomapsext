/* thumbnails.js begin */
//Thumbnail Functions (Full and Get-it display)

	var timeoutFuncs = []; //we need to delay the calculating of height/fileSize until ie has had time to calculate the page's display otherwise it returns 0s for everything.
	var imgLoadState; //for storing state of results.

	function thumbnailPop(row,img){
	  	//first we build an anonymous function. then we add it to the timeoutFuncs array. saving its index (the returned value)
	  	var timeoutFuncIndex = addTimeoutFunc( function(){
        	try{
           		if (imgLoadState!=1){ //if no image successfully loaded yet _in this row_, pop the image to the top. (assuming the image loaded a pic worth displaying.)
                	img.style.display = 'inline';
                	if (img.fileSize && img.fileSize != -1 && img.fileSize < 150){ //fix for ie7/6? if image is smaller than 150 bytes it's likely a transparent pixel.
                    	img.style.display = 'none';
                    	return;
                    }else if (img.height && img.height<10){
                    	img.style.display = 'none';
                    	return; //ignore very small images. (most likely errors like 1-pixel transparent gifs.)
                    }
                   	imgLoadState = 1;
                 	var p = img.parentNode;
            		p.className = "popThumbnailToTop "+p.className;
               	}
            }catch(ex){log(ex);}
        });
	  	//then we run the anonymous function from the timeoutFuncs array but only after 300 ms. giving the page time to initialize in ie.
        setTimeout('runTimeoutFunc('+timeoutFuncIndex+')',300); //note this is a 300 ms. delay after the image successfully fired the load event.
	}

    function runTimeoutFunc(index){
	    try{
	    	timeoutFuncs[index]();
	    	timeoutFuncs[index] = undefined;
	    }catch(ex){}
    }

    function addTimeoutFunc(func){
	    var success = false;
	    for(var i=0;i<30;i++){
	    	if (!timeoutFuncs[i]){
	        	timeoutFuncs[i] = func;
	          	return i;
	    	}
	    }
	    throw 'too many thumbnails to manage properly!';
    }

	function getGBSCover(covers){
		for (i in covers) {
			var cover = covers[i];
			if (cover){
				return cover.thumbnail_url;
			}
		}
		return undefined;
	}

	function thumbnailHide(img){
		var p = img.parentNode;
		p.style.display = 'none';
	}

	function updateGBSCover(covers){
		var thumbUrl = getGBSCover(covers);
		var thumbImg = e('googleThumb');
		if (thumbImg && thumbUrl){
			thumbImg.src = thumbUrl;
			//thumbImg.style.display = 'inline';
		}
	}

/* thumbnails.js end */