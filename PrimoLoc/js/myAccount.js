function editMyAccount(){
	var form = document.getElementById('personalSettingForm');
	form.reset();
	form.command.value = 'editPersonalSettings';
	form.submit();
	return false;
}

function editMyAccount(){
	var form = document.forms['personalSettingForm'];
	form.command.value = 'editPersonalSettings';
	form.submit();
	return false;
}

function settingSave(){
	var form = document.forms['personalSettingForm'];
	form.command  = 'displaySettingSave';
	form.submit();
	return false;
}

function displayConfirmation(anchorElement){
	document.getElementById('exliGreyOverlay').style.display='block';
	document.getElementById('exlidLighboxConfirmContainer').style.display='block';
	cancelUrl = anchorElement.href;
}

/**
* Called when the "Cancel" button from the confimation dialog is clicked
*/
function hideConfirmation() {
	document.getElementById('exliGreyOverlay').style.display='none';
	document.getElementById('exlidLighboxConfirmContainer').style.display='none';
}

/**
* Called when the "OK" button from the confimation dialog is clicked
*/
function confirm(okButton) {
	window.location=cancelUrl;
	okButton.disabled=true;
	document.getElementById('exlidLighboxConfirmButtonCancel').disabled=true;
}
