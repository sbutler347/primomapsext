<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en-GB" lang="en-GB" xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head">
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" href="css/redmond/jquery-ui-1.8.22.custom.css" rel="Stylesheet" />	
<link type="text/css" href="css/main.css" rel="Stylesheet"/>
 <script type="text/javascript" src="jquery-1.7.2.min.js"></script> <!-- jquery.min.js -->
 <script type="text/javascript" src="jquery-ui-1.8.22.custom.min.js"></script>  <!-- jquery.datepick.js -->
</head>
<body>

<?php
$collections = array(
	"MLD" => "6 Week Loan",
	"MLD7" => "1 Week Loan",
	"MLD7M" => "Lending Media 1 Week",
	"MLGOVD7" => "Gov.Pubs. 1 Week Loan",
	"MLGOVHAN"=>"Gov.Pubs.Hansard",
	"MLGOVREF" => "Main Library Gov.Pubs.Reference",
	"MLGOVSER" =>"Gov.Pubs.Serials",
	"MLGOVSEREF" => "Gov.Pubs. Serials Reference",
	"MLGPB" => "Giant Picture Books",
	"MLJ" => "Children's Books",
	"MLM" => "Media",
	"MLMAPREF" => "Map Area Reference",
	"MLMIC" => "Microform Lending - Ask Staff",
	"MLMICREF" => "Microform Reference - Ask Staff",
	"MLMICSER" => "Microform Serials - Ask Staff",
	"MLMUP" => "Music Parts",
	"MLMUS" => "Music Scores",
	"MLNEWS" => "Newspaper Shelves",
	"MLQREF" => "Quick Reference - Level 3 Enquiry Desk",
	"MLREF" => "Reference",
	"MLRSGS" => "Roy.Scot.Geog.Soc.Lending",
	"MLRSGSREF" => "Spec.Coll. Roy.Scot.Geog.Soc.Ref. - Ask Staff",
	"MLRSGSSER" => "Roy.Scot.Geog.Soc.Serials",
	"MLSCOPAR" => "Scottish Parliamentary Papers",
	"MLSEREF" => "Serials Reference",
	"MLSL1" => "Short Loan 1 Day Loan",
	"MLSL1M" => "Short Loan Media",
	"MLSER" => "624 Serial",
	"FLSER" =>"54 Serial",
	"JLSER" => "372.5 Serial",
	"JLSEREF" => "616.855 Serial",
	"JLTHEREF" => "Jordanhill Theses Reference",
	"LLSER" => "Law Serials Reference",
	"LLSEREF" => "Law Reports Reference",
	"ML ATSD" =>"Atlas Shelves Lending",
	"MLACQ" => "Andersonian Book Section",
	"MLATCD" => "Library Atlas Cases Lending",
	"MLATCREF" =>"Atlas Cases Reference",
	"MLATSREF" => "Atlas Shelves Reference",
	"MLSPAG" => "Special Collection Agnew - use in library only",
	"MLSPAL" => "Special Collection Aldred - use in Library only",
	"MLSPAN" => "Special Coll. Anderson - use in Library only",
	"MLSPAR" => "Spec. Coll. Architecture - use in Library only",
	"MLSPAU" => "Spec.Coll. Sutherland - use in Library only",
	"MLSPBR" => "Spec. Collection Branton - use in Library only",
	"MLSPBU" => "Spec. Coll. Buchanan Soc. - use in Library only",
	"MLSPCC" => "Special Coll. C. Carter - use in Library only",
	"MLSPDI" => "Special Coll. Dilettanti Soc. - use in Library",
	"MLSPFO" => "Spec. Coll. Food Science - use in Library only",
	"MLSPFS" => "Spec.Coll. Farm Serv. Union - use in Library only",
	"MLSPGA" => "Spec.Coll.German Atheneum - use in Library only",
	"MLSPGC" => "Special Collection Giles - use in Library only",
	"MLSPGE" => "Special Collection Geddes - use in Library only",
	"MLSPGI" => "Special Collection Gibson - use in Library only",
	"MLSPGL" => "Spec. Coll Glasgow Novel Collection - use in Library only",
	"MLSPHU" => "Special Collection Huxley - use in Library only",
	"MLSPJAZ" => "Spec.Coll. Waugh Jazz Records - use in Library only",
	"MLSPJE" => "Spec. Coll. John C.Eaton - use in Library only",
	"MLSPJW" => "Special Coll. J.Williams - use in Library only",
	"MLSPLA" => "Special Collection Laing - use in Library only",
	"MLSPLH" => "Spec.Coll. Labour History - use in Library only",
	"MLSPMA" => "Spec.Coll.Mechanics/Anderson - use in library only",
	"MLSPME" => "Special Collection Meehan - use in Library only",
	"MLSPMU" => "Spec. Collection Muirhead - use in Library only",
	"MLSPNC" => "Special Collection NCLC - use in Library only",
	"MLSPNI" => "Special Collection Nicoll - use in Library only",
	"MLSPNT" => "Special Coll. New Towns - use in Library only",
	"MLSPOC" => "Special Collection OEDA - use in Library only",
	"MLSPPA" => "Spec. Coll. Palaeontology - use in Library only",
	"MLSPPIN" => "Spec. Coll. Pinkerton - use in Library only",
	"MLSPRA" => "Special Coll. Rare Book - use in Library only",
	"MLSPRO" => "Special Coll.Robertson - use in Library only",
	"MLSPRP" => "Spec.Coll. Roy.Phil.Soc. - use in Library only",
	"MLSPSA" => "Spec.Coll. Sc.Hot.Sch.Antiquar. - use in Library",
	"MLSPSC" => "Spec.Coll.Sc.Hot.Sch.Cookery - use in Library only",
	"MLSPSER" => "Special Coll. Serials - use in Library only",
	"MLSPSM" => "Special Collection SMC - use in Library only",
	"MLSPSO" => "Strathclyde Coll.Official - use in Library only",
	"MLSPSP" => "Spec. Coll. Election Ephemera",
	"MLSPSS" => "Strathclyde Coll. Staff - use in Library only",
	"MLSPSX" => "Spec. Coll. Stock Exchange - use in Library only",
	"MLSPYO" => "Special Collection Young - use in Library only",
	"MLSTD" => "Store Lending - Make request",
	"MLSTSER" => "Store Serials - Ask Staff",
	"MLSTU" => "Store Lending - Make request",
	"MLTHE" => "Theses Lending - Make request",
	"MLTHEMOR" => "Theses Moratorium",
	"MLTHEREF" => "Theses Reference - Make request",
	"MLV" => "DVD/CD(ROM)",
	"MLV7" => "DVD/CD(ROM) 1 Week",
	"SERJL" => "Serials",
	"SERML" =>"Andersonian Serials Sect.",
	);
	
?>
<script>
var titleJ;
</script>
<?php	

$title = "One secret thing";
$testLoc = "D 882.8-9 SOL/S";
$tidyLoca = preg_replace('/[^0-9.]/',"", $testLoc);
echo '<script>';
echo 'var tidyLoca = '.$tidyLoca.';';
echo 'var title = "'.$title.'";';
echo '</script>';
?>
</br>


<select name="collections" id="collections" size="1">; 
<option selected="selected" disabled>Select collection to view</option>
<?php
foreach ($collections as $key=>$value){
echo '<option  value="'.$key.'" >'.$value.'</option>';
}
echo '<option selected="selected" disabled></option>';

echo '</select>';
?>
<script>
var javaCollections= new Array;</script>
<?php
foreach ($collections as $value){
echo '<script>javaCollections.push ("'.$value.'");</script>';} ?>

<script>

var activeColl = new Array("Map Area Reference", "Lending Media 1 Week", "6 Week Loan", "Andersonian Book Section","Special Collection Huxley - use in Library only", "Spec.Coll. Waugh Jazz Records - use in Library only", "Spec. Coll. John C.Eaton - use in Library only",
 "Special Coll. J.Williams - use in Library only", "Special Collection Laing - use in Library only", "Spec.Coll. Labour History - use in Library only",
 "Spec.Coll.Mechanics/Anderson - use in library only", "Special Collection Meehan - use in Library only", "Spec. Collection Muirhead - use in Library only",
 "Special Collection NCLC - use in Library only", "Special Collection Nicoll - use in Library only", "Special Coll. New Towns - use in Library only",
"Special Collection OEDA - use in Library only", "Spec. Coll Glasgow Novel Collection - use in Library only");



var collList = new Array;
var finalList = new Array;

 "Special Coll. Dilettanti Soc. - use in Library"			
$(document).ready(function(){
				$("option").wrap('<div>').hide()});

				
			
for(c=0;c<finalList.length;c++){
		document.write("<br />"+finalList[c]);
	}
</script>
			

<script>
$(document).ready(function(){
for(i=0;i<=activeColl.length;i++){
var currStr = activeColl[i];
		$("option:contains("+currStr+")").unwrap();

		}$('#clicky').hide();
});


locNm = tidyLoca;

var coll;
$('#collections').change(function(){
coll=collections.value;
aftSel(coll);
document.getElementById('hldFld').value=locNm;
}
);
var collection ="MLD";
function aftSel(coll){
collection =coll;
document.getElementById('clFld').value=collection;

}
</script>
<form method="post" action="husk.php">
<input type="text" name="tidyLoca" id="hldFld" readonly>
<input type="text" name="collection" id="clFld" readonly>
<input type="submit" value="Submit" name="submitBtn" >
</form>

</body>
</html>