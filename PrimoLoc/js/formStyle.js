// formStyle.js, produced by Philip Howard, GamingHeadlines.co.uk
// This JavaScript is open source and freely available to all those who wish to use it.
// A link back to GamingHeadlines.co.uk would be appreciated!

	function toggleCheckbox(cbId,cbKey,ffId)
	{
	if (cbKey==0||cbKey==32){
	var cbFF = document.getElementById(ffId);
	
		var cbFFValue = cbFF.checked;
		
		if(cbId.className.indexOf("CheckboxChecked")<0)
			{
				var checkBoxType = cbId.className.replace("CheckboxUnchecked","");
				cbFF.checked=true;cbId.className="CheckboxChecked"+checkBoxType;}
			else
			{
				var checkBoxType = cbId.className.replace("CheckboxChecked","");
				cbFF.checked=false;cbId.className="CheckboxUnchecked"+checkBoxType;}
	return false;
		}
	}
	
	function InitialiseCheckboxes()
	{
		var inputFields = document.getElementsByTagName("span");
		var checkboxIndex = 0;
		for (var inputIndex=0;inputIndex<inputFields.length;inputIndex++)
			{
				if (inputFields[inputIndex].className.indexOf("cbStyled")==0)
					{
						var styleType = "";
						//if (inputFields[inputIndex].getAttribute("name")!=null){styleType=inputFields[inputIndex].getAttribute("name");}
						styleType=inputFields[inputIndex].className.replace("cbStyled ","");
						if(styleType=="cbStyled"){styleType="";};
						inputFields[inputIndex].className="Checkbox"+styleType;
						
						var inputCurrent = inputFields[inputIndex].getElementsByTagName("input").item(0);
						if(inputCurrent.getAttribute("type")=="checkbox")
						{
							inputCurrent.className = "InputHidden";
							inputCurrent.setAttribute("id","StyledCheckbox"+checkboxIndex);
							
							if(navigator.appName.indexOf("Internet Explorer")>0||navigator.userAgent.indexOf("Netscape")>0)
							{
								//Internet Explorer
								var inputHTML = inputFields[inputIndex].innerHTML;
								var styledHTML = "<a"//href=\"#\""
								styledHTML+=" tabindex=\""+inputIndex+"\"";
								//styledHTML+=" name=\""+inputCurrent.getAttribute("name")+"\""
								
								if(inputCurrent.hasAttribute){if(inputCurrent.hasAttribute("title")){styledHTML+=" title=\""+inputCurrent.getAttribute("title")+"\"";}}
								
								if (inputCurrent.checked)
									{styledHTML+=" class=\"CheckboxChecked"+styleType+"\""}
									else
									{styledHTML+=" class=\"CheckboxUnchecked"+styleType+"\""}
									
								styledHTML+=" onClick=\"toggleCheckbox(this,'','StyledCheckbox"+checkboxIndex+"');return false;\""
								styledHTML+=" onKeyPress=\"return toggleCheckbox(this,event.keyCode,'StyledCheckbox"+checkboxIndex+"');\""
								
								if(navigator.userAgent.indexOf("Netscape")>0)
								{styledHTML+="><img src=\"formStyle.gif\" /></a>"}
								else
								{styledHTML+="></a>"}

								inputFields[inputIndex].innerHTML = inputHTML+styledHTML;
								inputFields[inputIndex].className = "Radiobox"+styleType;
							}
							else
							{
								var styledCheckbox = document.createElement("a"); 
								styledCheckbox.setAttribute("href","#");
								
								if(inputCurrent.hasAttribute){if(inputCurrent.hasAttribute("title")){styledCheckbox.setAttribute("title",inputCurrent.getAttribute("title"));}}
								
								styledCheckbox.setAttribute("onClick","toggleCheckbox(this,'','StyledCheckbox"+checkboxIndex+"');return false;");
								styledCheckbox.setAttribute("onKeyPress","return toggleCheckbox(this,event.keyCode,'StyledCheckbox"+checkboxIndex+"');");
								
								if (inputCurrent.checked)
									{styledCheckbox.className="CheckboxChecked"+styleType;}
									else
									{styledCheckbox.className="CheckboxUnchecked"+styleType;}
								inputFields[inputIndex].appendChild(styledCheckbox);
							}
							
							checkboxIndex++;
						}
					}
			}	
	}
	
	
	function checkImages() {
	  if (document.getElementById) {
		var x = document.getElementById('formStyleTestImage').offsetWidth;
		if (x == '1'||x == '7') {
			document.getElementById('formStyleTestImage').style.display='none';
			return true;
		}else{
			return false;
		}
	  }
	}
	
	function preloadImages()
		{
		img1 = new Image();img1.src = "CheckboxUnchecked.gif";
		img1 = new Image();img1.src = "CheckboxChecked.gif";
		}
	
	function Initialise()
		{
			if(checkImages()){preloadImages();InitialiseCheckboxes();}
		}
		
	window.onload = Initialise;