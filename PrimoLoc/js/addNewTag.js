<script type="text/javascript">
		function save(){
			document.getElementById('tagsForm').submit();
		}

		function initTags(){
			var chkboxArr = document.getElementsByTagName("input");
			for (var i = 0; i<chkboxArr.length; i++){
				if (/\btagsChx\b/.exec(chkboxArr[i].className) && chkboxArr[i].name.indexOf('my_tag_')!=-1){
					assign(chkboxArr[i].value, chkboxArr[i].checked, true);
				}
			}

		}

		function assign(value, state, isOnLoad)
		{
			var chkboxArr = document.getElementsByTagName("input");
			var chkboxArr_results = [];
			for (var i = 0; i<chkboxArr.length; i++)
				if (/\btagsChx\b/.exec(chkboxArr[i].className))
					chkboxArr_results[chkboxArr_results.length] = chkboxArr[i];

			obj = document.getElementById("tags_input");
			string = obj.value;
			found = false;

			if(state) // adding to string
			{
				if(string.length!=0)
				{
					strArr = string.split(",");
		 			for(i=0;i< strArr.length;i++)
		 			{
		 				if(value==strArr[i])
						{
							found = true;
						 	continue;
						}
					}
					if(!found) string += ","+value;
				}
				else string += value;

				for(a=0;a<chkboxArr_results.length;a++)
					if((chkboxArr_results[a].value==value)&&(chkboxArr_results[a].checked==false))
						chkboxArr_results[a].checked = true;
			}
			else // removing from string
			{
				strArr = string.split(",");
				for(i=0;i< strArr.length;i++)
				{
					if(value==strArr[i])
					{
						len =(strArr[i].length)+1;
						stringlen = string.length;
						if(i==0) // first in line
						{
							string = string.substr(len, stringlen);
						}
						else if(strArr[strArr.length-1]==value)
						{
							loc = string.indexOf(strArr[i]);
							string = string.substr(0, loc-1);
						}
						else // middle of line
						{
							loc = string.indexOf(strArr[i]);
							temp01 = string.substr(0, loc);
							temp02 = string.substr((temp01.length+len), stringlen);
							string = temp01 + temp02;
						}
					}
				}
				if(string.length==(strArr[0].length+2))
				 	string = string.substr(0,(string.length-2));

				for(a=0;a<chkboxArr_results.length;a++)
					if((chkboxArr_results[a].value==value)&&(chkboxArr_results[a].checked==true))
						chkboxArr_results[a].checked = false;
			}

			if (!isOnLoad){
				obj.value = string;
			}
		}

		/* tooltip */

		function show(obj)
		{
	        		document.getElementById(obj).style.visibility = 'visible';
		}

		function hide(obj)
		{

			document.getElementById(obj).style.visibility = 'hidden';

		}

		function findPosX(obj)
		{
			var curleft = 0;
			if (obj.offsetParent)
			{
				while (obj.offsetParent)
				{
					curleft += obj.offsetLeft
					obj = obj.offsetParent;
				}
			}
			else if (obj.x)
				curleft += obj.x;
			return curleft;
		}

		function findPosY(obj)
		{
			var curtop = 0;
			if (obj.offsetParent)
			{
				while (obj.offsetParent)
				{
					curtop += obj.offsetTop
					obj = obj.offsetParent;
				}
			}
			else if (obj.y)
				curtop += obj.y;
			return curtop;
		}

		function browserDetect(text) {
		   str = (navigator.userAgent.toLowerCase()).indexOf(text) + 1;
		   data = text;
		   return str;
		}

	</script>