<?php
echo "<script src='//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js'></script>";
echo " <link rel='stylesheet' href='css/mapStyle.css' TYPE='text/css'>";
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
$title = $_GET['title'];
$callno= $_GET['callno'];
$collection = $_GET['collection'];
$author= $_GET['author'];

$collections = array(
 "6 Week Loan" =>"MLD", 
 "Main Library 6 Week Loan"=>"MLD", 
 "1 Week Loan"=>"MLD7" , 
 "Lending Media 1 Week"=> "MLD7M",
 "Gov.Pubs. 1 Week Loan" => "MLGOVD7", 
 "Gov.Pubs.Hansard" => "MLGOVHAN",
 "Gov.Pubs.Reference" => "MLGOVREF", 
 "Gov.Pubs.Serials" => "MLGOVSER",
 "Gov.Pubs. Serials Reference" => "MLGOVSEREF",
 "Giant Picture Books"=> "MLGPB",
 "Children's Books" => "MLJ",
 "Media"=> "MLM","Map Area Reference" =>"MLMAPREF",
 "Microform Lending - Ask Staff"=>"MLMIC",
 "Microform Reference - Ask Staff"=>"MLMICREF",	
 "Microform Serials - Ask Staff"=>"MLMICSER" , 
	"Music Parts" =>"MLMUP",
	"Music Scores"=>"MLMUS",
	"Newspaper Shelves" =>"MLNEWS",	
	"Quick Reference - Level 3 Enquiry Desk" => "MLQREF",
	"Reference" => "MLREF",
	"Roy.Scot.Geog.Soc.Lending"=>"MLRSGS",
	"Spec.Coll. Roy.Scot.Geog.Soc.Ref. - Ask Staff"=> "MLRSGSREF",
	"Roy.Scot.Geog.Soc.Serials" => "MLRSGSSER",
	"Scottish Parliamentary Papers" => "MLSCOPAR",
	"Serials Reference" => "MLSEREF",
	"Short Loan 1 Day Loan"=> "MLSL1",
	"Short Loan Media Reference"=>"MLSL1M",
	"Serials" =>"MLSER",
	"Jordanhill Serials Reference" => "JLSEREF",
	"Jordanhill Theses Reference" => "JLTHEREF",	
	"Law Serials Reference"=>"LLSER",	
	"Law Reports Reference" =>"LLSEREF",
	"Atlas Shelves Lending" =>"MLATSD",	
	"Andersonian Book Section" =>"MLACQ",
	"Library Atlas Cases Lending" =>"MLATCD",	
	"Atlas Cases Reference" =>"MLATCREF",	
	"Atlas Shelves Reference" =>"MLATSREF",
	"Special Collection Agnew - use in library only" =>"MLSPAG",
	"Special Collection Aldred - use in Library only" =>"MLSPAL",	
	"Special Coll. Anderson - use in Library only"=>"MLSPAN" ,
	"Spec. Coll. Architecture - use in Library only"=>"MLSPAR" ,
	"Spec.Coll. Sutherland - use in Library only"=>"MLSPAU",
	"Spec. Collection Branton - use in Library only" =>"MLSPBR",
	"Spec. Coll. Buchanan Soc. - use in Library only" =>"MLSPBU",
	"Special Coll. C. Carter - use in Library only" =>"MLSPCC",
	"Special Coll. Dilettanti Soc. - use in Library" =>"MLSPDI" ,	
	"Spec. Coll. Food Science - use in Library only"=>"MLSPFO2" ,
	"Special Collection Food Science - use in Library only"=>"MLSPFO" ,	
	"Spec.Coll. Farm Serv. Union - use in Library only" =>"MLSPFS",	
	"Spec.Coll.German Atheneum - use in Library only" => "MLSPGA",
	"Special Collection Giles - use in Library only" =>"MLSPGC",
	"Special Collection Geddes - use in Library only"=> "MLSPGE",
	"Special Collection Gibson - use in Library only" => "MLSPGI",
	"Spec. Coll Glasgow Novel Collection - use in Library only" =>"MLSPGL",	
	"Special Collection Huxley - use in Library only" => "MLSPHU",
	"Spec.Coll. Waugh Jazz Records - use in Library only" =>"MLSPJAZ",
	"Spec. Coll. John C.Eaton - use in Library only" => "MLSPJE",
	"Special Coll. J.Williams - use in Library only" => "MLSPJW",	
	"Special Collection Laing - use in Library only" =>"MLSPLA",
	"Spec.Coll. Labour History - use in Library only" =>"MLSPLH",
	"Spec.Coll.Mechanics/Anderson - use in library only" =>"MLSPMA",
	"Special Collection Meehan - use in Library only" =>"MLSPME",
	"Spec. Collection Muirhead - use in Library only" =>"MLSPMU" ,	
	"Special Collection NCLC - use in Library only" =>"MLSPNC",	
	"Special Collection Nicoll - use in Library only" => "MLSPNI",
	"Special Coll. New Towns - use in Library only" =>"MLSPNT",
	"Special Collection OEDA - use in Library only" =>"MLSPOC",	
	"Spec. Coll. Palaeontology - use in Library only" => "MLSPPA",
	"Spec. Coll. Pinkerton - use in Library only" => "MLSPPIN",	
	"Special Coll. Rare Book - use in Library only" => "MLSPRA",
	"Special Coll.Robertson - use in Library only" =>"MLSPRO",
	"Spec.Coll. Roy.Phil.Soc. - use in Library only" =>"MLSPRP" ,
	"Spec.Coll. Sc.Hot.Sch.Antiquar. - use in Library" =>"MLSPSA",
	"Special Collection Sc.Hot.Sch.Cookery - use in Library only" =>"MLSPSC",
	"Special Coll. Serials - use in Library only" =>"MLSPSER",
	"Special Collection SMC - use in Library only" =>"MLSPSM",	
	"Strathclyde Coll.Official - use in Library only" =>"MLSPSO",
	"Spec. Coll. Election Ephemera" =>"MLSPSP" ,
	"Strathclyde Coll. Staff - use in Library only" =>"MLSPSS",
	"Special Collection Stock Exchange - use in Library only" => "MLSPSX",
	"Special Collection Young - use in Library only" =>"MLSPYO",
	"Store Lending - Make Store Lending Request" =>"MLSTD" ,
	"Store Serials - Make Store Journal Request" =>"MLSTSER",
	"Store Audiovisual Reference - Ask Staff" => "MLAVREF",	
	"Store Lending - Make Store Lending Request" =>"MLSTU" ,
	"Theses Lending - Make Store Lending Request" =>"MLTHE",
	"Theses Moratorium" => "MLTHEMOR",	
	"Theses Reference - Make request" =>"MLTHEREF",	"Remote Store - Ask Staff" =>"MLSTOREM",
	"Andersonian Serials Sect." =>"SERML",
	"Reference" => "MLCDROMREF",
	"Lending - UDC books" =>"MLU",	
	"Short Loan 3 Hour Loan" =>"MLSL3H",
	"Serials Jordanhill Library" =>"SERJL" ,
	"DVD/CD(ROM)" =>"MLV",
	"Theses Reference - Make Store Lending Request"=>"MLTHEREF",
	);
	$callno =  preg_replace("/[^0-9.]/", "", $callno);
	if($callno == ''){
		$callno = 1;
		$showCall = false;
		}
	else{
		$showCall =true;
		}
#	$collections=array_flip($collections);
	$id = $collections[$collection];

	$db = new SQLite3('db/stacks.db');
	$stmt = $db->prepare("Select i.image_url, i.floor from map_image i join maps m on m.map_image = i.id join collection c on c.id = m.collection join call_number b on b.id = m.call_number  where c.collection = ? AND b.begin_call_number < ? AND b.end_call_number > ?");
	$stmt->bindParam(1, $id);
	$stmt->bindParam(2, $callno);
	$stmt->bindParam(3, $callno);
echo "<div id='logo'><img src='http://lib-vmsweb2.lib.strath.ac.uk/html/maps/images/logo.gif' alt='SUPrimo banner - University of Strathclyde Library'></div><br />";
echo "<div id='content'><div id='inner'><div id='clickable'></div><span class='metadata'><span class='label'>Title: </span>".$title."</span>";
echo "<br/><span class='metadata'><span class='label'>Author: </span>".$author."</span><br/>";
if($showCall != false){
echo"<span class='metadata'><span class='label'>Call Number: </span>".$callno."</span><br/>";
} 
echo "<span class='metadata'><span class='label'>Collection: </span>".$collection."</span>";
	
	$result =$stmt->execute();
	while($res = $result->fetchArray()){
	echo "<br/><span class='metadata'><span class='label'>Floor: </span>". $res[1]."</span><br/></div></div>";

		echo "<br/><img class ='mapImg' src='".$res[0]."'></img><br/>";
		
		}
	echo "<hr>";
	#var_dump($stmt);
	

?>
