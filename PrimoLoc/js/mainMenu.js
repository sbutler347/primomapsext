<script type="text/javascript">
//<![CDATA[
function loadSearchDbPage(){
	var fn;
	addLightBoxDivs();
	fn='loadSearchDbPage';
	var timestamp = new Date().getTime();
	var url = "searchDB.do"+"?fn="+fn+"&ts="+timestamp;
	var data;
	$.ajax({
		url:url,
		data:data,
		global:false,
		beforeSend: function(request){
			setAjaxRequestHeader(request);
		},
		error: function(request,errorType,exceptionOcurred){
			if (errorType=='timeout'){
				notifyAjaxTimeout();
			}else{
				generalAjaxError();
			}
			document.getElementById('exliLoadingFdb').style.display='none';
			document.getElementById('exliGreyOverlay').style.display='none';
			document.getElementById('exliWhiteContent').style.display='none';
			return false;
		},
		success: function(data){
			if (isAjaxXmlRedirect(data)){
				handleAjaxXmlRedirect(data);
			}
			var elm = $(data).find('searchDbXml');
			var cdata = $(elm).text();
			$('span[id=exliLoadingFdbSpan]').remove();
			var n;
			n = document.getElementById("exliWhiteContent");
			var xmlText = cdata.replace(/\n\n/g, "\n").replace(/&lt;/g,"<").replace(/&gt;/g,">");
			var newdiv = document.createElement("div");
			newdiv.innerHTML = xmlText;
			n.innerHTML='';
			$(n).append(newdiv);
		}
	});
}

function addLightBoxDivs(){

	var greyOutLine = document.getElementById('exliGreyOverlay');
	if($('#exliGreyOverlay').size() == 0){
		var greyOutLine = document.createElement("div");
		$(greyOutLine).attr("id","exliGreyOverlay");
		$(greyOutLine).attr("style","display: none;");
	}

	var greyOutLineIframe = document.createElement("iframe");
	$(greyOutLineIframe).attr("src","javascript:false;");

	var whiteContent = document.getElementById('exliWhiteContent');
	if($('#exliWhiteContent').size() == 0){
		var whiteContent = document.createElement("div");
		$(whiteContent).attr("id","exliWhiteContent");
		$(whiteContent).attr("style","display: none;");
	}

	var loadingFdb = document.getElementById('exliLoadingFdb');
	if($('#exliLoadingFdb').size() == 0){
		var loadingFdb = document.createElement("div");
		$(loadingFdb).attr("id","exliLoadingFdb");
		$(loadingFdb).attr("style","display: block;");
		$(loadingFdb).attr("class","exliLoadingFdb");
	}
	var loadingFdbInnerHtml = '<span class="EXLHiddenCue" id="exliLoadingFdbSpan">'+
	exlFdbReloadMessage+
	'<a href="#" onclick="document.getElementById('+
	"'exliLoadingFdb').style.display='none';document.getElementById('exliGreyOverlay').style.display='none';return false;"+
	'">'+exlFdbReloadLinkText+'<\/a>.'+
	"<script type='text/javascript'>"+
	"setTimeout('$("+
	'"#exliLoadingFdbSpan").removeClass("EXLHiddenCue").attr("style","display: block;");'+
	"', 10000);<\/script>"+
	'<\/span><iframe src="javascript:false;"><\/iframe>';
	$(loadingFdb).html(loadingFdbInnerHtml);
	$(greyOutLine).html(greyOutLineIframe);

	$('body')[0].appendChild(greyOutLine);
	$('body')[0].appendChild(whiteContent);
	$('body')[0].appendChild(loadingFdb);
	document.getElementById('exliLoadingFdb').style.display='block';
	document.getElementById('exliLoadingFdbSpan').style.display='none';
	document.getElementById('exliGreyOverlay').style.display='block';

}

// ]]>
</script>