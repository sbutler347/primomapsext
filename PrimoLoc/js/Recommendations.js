<!-- this code is no longer in use.... thankfully! @hanan -->
<script type="text/javascript">

	function hideTab()
	{
		if (document.getElementById('exlidHref${recordResultIndex}')) {
			document.getElementById('exlidHref${recordResultIndex}').title = "<fmt:message key="recommendationtab.norecommendation"/>";
		}
	}

	function showTab()
	{
		$('#exlidResult${recordResultIndex}-RecommendTab').show();
		var tabsContainer = $(document.getElementById('exlidResult${recordResultIndex}-RecommendTab')).parents('.EXLResult').find('.EXLContainer-'+'recommendTab').get(0);
		tabsContainer.tabUtils.state.status = exlTabState.UNFETCHED;

		if (document.getElementById('exlidHref${recordResultIndex}')) {
			document.getElementById('exlidHref${recordResultIndex}').title = "<fmt:message key="recommendationtab.recommendations_found"/>";
		}
	}

</script>
