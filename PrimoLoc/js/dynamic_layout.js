
$().ready(function(){
	shareDivHeight('.EXLHeightSharingContainer');
	borrowDivHeight('.EXLHeightBorrowingContainer');

});

function shareDivHeight(className){ //checks the height
	$(className).each(function(){
			var maxHeight = 0;
			$(this).children().each(function(){
				if ($(this).height()>maxHeight){
					maxHeight = $(this).height();
					//log("maxHeight: " + maxHeight);
				}
			}).height(maxHeight);
	});
}

function borrowDivHeight(className){ //checks the height
	$(className).each(function(){
			var maxHeight = 0;
			$(this).children(':not(.EXLBorrowHeight)').each(function(){
				if ($(this).height()>maxHeight){
					maxHeight = $(this).height();
					//log("maxHeight: " + maxHeight);
				}
			}).end().children('.EXLBorrowHeight').height(maxHeight);
	});
}


$(window).resize(function(){
	$('.EXLHeightSharingContainer').each(function(){
		$(this).children().height(''); //this can ruin a layout where one of the divs started out with a styled height.
	});
	shareDivHeight('.EXLHeightSharingContainer');

	borrowDivHeight('.EXLHeightBorrowingContainer');
});