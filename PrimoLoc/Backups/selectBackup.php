<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en-GB" lang="en-GB" xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head">
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" href="css/redmond/jquery-ui-1.8.22.custom.css" rel="Stylesheet" />	
<link type="text/css" href="css/main.css" rel="Stylesheet"/>
 <script type="text/javascript" src="jquery-1.7.2.min.js"></script> <!-- jquery.min.js -->
 <script type="text/javascript" src="jquery-ui-1.8.22.custom.min.js"></script>  <!-- jquery.datepick.js -->
</head>
<body>

<?php
$collections = array(
	"MLD" => "Main Library 6 Week Loan",
	"MLD7" => "Main Library 1 Week Loan",
	"MLD7M" => "Lending Media 1 Week",
	"MLGOVD7" => "Main Library Gov.Pubs. 1 Week Loan",
	"MLGOVHAN"=> "Main Library Gov.Pubs.Hansard",
	"MLGOVREF" => "Main Library Gov.Pubs.Reference",
	"MLGOVSER" =>"Main Library Gov.Pubs.Serials",
	"MLGOVSEREF" => "Gov.Pubs.Serials Reference",
	"MLGPB" => "Giant Picture Books",
	"MLJ" => "Children's Books",
	"MLM" => "Media",
	"MLMAPREF" => "Main Library Map Area Reference",
	"MLMIC" => "Main Library Microform Lending - Ask Staff",
	"MLMICREF" => "Main Library Microform Reference - Ask Staff",
	"MLMICSER" => "Main Library Microform Serials - Ask Staff",
	"MLMUP" => "Music Parts",
	"MLMUS" => "Music Scores",
	"MLNEWS" => "Main Library Newspaper Shelves",
	"MLQREF" => "Main Library Quick Reference - Level 3 Enquiry Desk",
	"MLREF" => "Main Library Reference",
	"MLRSGS" => "Roy.Scot.Geog.Soc.Lending",
	"MLRSGSREF" => "Main Library Spec.Coll. Roy.Scot.Geog.Soc.Ref. - Ask Staff",
	"MLRSGSSER" => "Roy.Scot.Geog.Soc.Serials",
	"MLSCOPAR" => "Main Library Scottish Parliamentary Papers",
	"MLSEREF" => "Main Library Serials Reference",
	"MLSL1" => "Main Library Short Loan 1 Day Loan",
	"MLSL1M" => "Short Loan Media",
	"MLSER" => "624 Serial",
	"FLSER" =>"Main Library Serials",
	"JLSEREF" => "Jordanhill Serials Reference",
	"JLTHEREF" => "Jordanhill Theses Reference",
	"LLSER" => "Law Serials Reference",
	"LLSEREF" => "Law Reports Reference",
	"ML ATSD" =>"Main Library Atlas Shelves Lending",
	"MLACQ" => "Andersonian Book Section",
	"MLATCD" => "Main Library Atlas Cases Lending",
	"MLATCREF" =>"Main Library Atlas Cases Reference",
	"MLATSREF" => "Main Library Atlas Shelves Reference",
	"MLSPAG" => "Main Library Special Collection Agnew - use in library only",
	"MLSPAL" => "Main Library Special Collection Aldred - use in Library only",
	"MLSPAN" => "Main Library Special Coll. Anderson - use in Library only",
	"MLSPAR" => "Main Library Spec. Coll. Architecture - use in Library only",
	"MLSPAU" => "Main Library Spec.Coll. Sutherland - use in Library only",
	"MLSPBR" => "Main Library Spec. Collection Branton - use in Library only",
	"MLSPBU" => "Main Library Spec. Coll. Buchanan Soc. - use in Library only",
	"MLSPCC" => "Main Library Special Coll. C. Carter - use in Library only",
	"MLSPDI" => "Main Library Special Coll. Dilettanti Soc. - use in Library",
	"MLSPFO" => "Main Library Spec. Coll. Food Science - use in Library only",
	"MLSPFS" => "Main Lib. Spec.Coll. Farm Serv. Union - use in Library only",
	"MLSPGA" => "Main Library Spec.Coll.German Atheneum - use in Library only",
	"MLSPGC" => "Main Library Special Collection Giles - use in Library only",
	"MLSPGE" => "Main Library Special Collection Geddes - use in Library only",
	"MLSPGI" => "Main Library Special Collection Gibson - use in Library only ",
	"MLSPGL" => "ML Spec. Coll Glasgow Novel Collection - use in Library only",
	"MLSPHU" => "Main Library Special Collection Huxley - use in Library only",
	"MLSPJAZ" => "Main Library Spec.Coll. Waugh Jazz Records - use in Library only",
	"MLSPJE" => "Main Library Spec. Coll. John C.Eaton - use in Library only",
	"MLSPJW" => "Main Library Special Coll. J.Williams - use in Library only",
	"MLSPLA" => "Main Library Special Collection Laing - use in Library only",
	"MLSPLH" => "Main Library Spec.Coll. Labour History - use in Library only",
	"MLSPMA" => "Main Lib. Spec.Coll.Mechanics/Anderson - use in library only ",
	"MLSPME" => "Main Library Special Collection Meehan - use in Library only",
	"MLSPMU" => "Main Library Spec. Collection Muirhead - use in Library only",
	"MLSPNC" => "Main Library Special Collection NCLC - use in Library only",
	"MLSPNI" => "Main Library Special Collection Nicoll - use in Library only",
	"MLSPNT" => "Main Library Special Coll. New Towns - use in Library only",
	"MLSPOC" => "Main Library Special Collection OEDA - use in Library only",
	"MLSPPA" => "Main Lib. Spec. Coll. Palaeontology - use in Library only",
	"MLSPPIN" => "Main Library Special Coll. Pinkerton - use in Library only",
	"MLSPRA" => "Main Library Special Coll. Rare Book - use in Library only",
	"MLSPRO" => "Main Library Special Coll.Robertson - use in Library only",
	"MLSPRP" => "Main Library Spec.Coll. Roy.Phil.Soc. - use in Library only",
	"MLSPSA" => "Main Lib. Spec.Coll. Sc.Hot.Sch.Antiquar. - use in Library",
	"MLSPSC" => "Main Lib.Spec.Coll.Sc.Hot.Sch.Cookery - use in Library only",
	"MLSPSER" => "Main Library Special Coll. Serials - use in Library only",
	"MLSPSM" => "Main Library Special Collection SMC - use in Library only",
	"MLSPSO" => "Main Library Strathclyde Coll.Official - use in Library only",
	"MLSPSP" => "Main Library Spec. Coll. Election Ephemera",
	"MLSPSS" => "Strathclyde Coll. Staff - use in Library only",
	"MLSPSX" => "Main Library Spec. Coll. Stock Exchange - use in Library only",
	"MLSPYO" => "Main Library Special Collection Young - use in Library only ",
	"MLSTD" => "Store Lending - Make request",
	"MLSTSER" => "Main Library Store Serials - Ask Staff",
	"MLAVREF" =>"ML Store Audiovisual Reference - Ask Staff",
	"MLSTU" => "Main Library Store Lending - Make request",
	"MLTHE" => "Main Library Theses Lending - Make request",
	"MLTHEMOR" => "Main Library Theses Moratorium",
	"MLTHEREF" => "Main Library Theses Reference - Make request",
	"MLNCS" => "Serials**Closed",
	"MLSTOREM" => "Main Library Remote Store - Ask Staff",
	"SERML" =>"Andersonian Serials Sect.",
	"MLCDROMREF" => "Store Audiovisual CDROM Reference - Ask Staff",
	"MLU" => "Lending - UDC books",
	"MLSL3H" => "Short Loan 3 Hour Loan",
	"MLV" => "DVD/CD(ROM)",
	"MLV7" => "DVD/CD(ROM) 1 Week",
	
	);
	
?>


<?php	
$testLoc = ($_POST['tidyLoc']);
$tidyLoca = preg_replace('/[^0-9.]/',"", $testLoc);
$newCol = ($_POST['collectionP']);
$callNoLength = strlen($testLoc);
echo '<script>';
echo 'var tidyLoca = '.$tidyLoca.'';
echo '</script>';
session_start();
$_SESSION['callNo'] = $tidyLoca ; 
$_SESSION['collNo'] = $newCol  ; 


?>
</br>
<script>
var newCol = "<?php echo $newCol; ?>";
var activeColls = newCol.split(',');

</script>
<select name="collections" id="collections" size="1">; 
<option selected="selected" disabled>Select collection to view</option>

<?php
$browser= $_SERVER['HTTP_USER_AGENT'] . "\n\n";
if(strpos($browser, 'MSIE') !== false){
echo 'true';
  header("Location: http://kattrin.mis.strath.ac.uk/PrimoLoc/ieSelect.php"); 
exit;
}
?>
<?php
foreach ($collections as $key=>$value){
echo '<option id="hidOpt"  value="'.$key.'">'.$value.'</option>';
}
echo '</select>';
?>
<script>
var javaCollections= new Array;</script>
<?php
;
foreach ($collections as $value){
echo '<script>javaCollections.push ("'.$value.'");</script>';} ?>

<script>
var callNoLenJ = <?php echo $callNoLength; ?>;
var collList = new Array;
var finalList = new Array;
for (i=0; i<activeColls.length;i++){
	currStr = activeColls[i];


	for(b=0;b<javaCollections.length;b++){
		if (javaCollections[b].match (currStr)){

		$("option:contains("+javaCollections[b]+")").css("display","block");
				}
			}
		}	
for(c=0;c<finalList.length;c++){
		document.write("<br />"+finalList[c]);
	}
</script>

<script>
if(callNoLenJ == 0){
locNm = 1;}
if(callNoLenJ != 0){
locNm = tidyLoca;
}

var coll;
$('#collections').change(function(){
coll=collections.value;
aftSel(coll);
document.getElementById('hldFld').value=locNm;
}
);
var collection ="MLD";
function aftSel(coll){
collection =coll;
document.getElementById('clFld').value=collection;
}
</script>

<form method="post" action="husk.php">
<input type="text" name="tidyLoca" id="hldFld" readonly>
<input type="text" name="collection" id="clFld" readonly>
<input type="submit" value="Submit" name="submit" >
</form>
<?php
session_unset();
session_destroy();?>
</body>
</html>