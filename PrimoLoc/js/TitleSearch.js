<script type="text/javascript"> // <![CDATA[

function removeChildren(elm){
	if ( elm.hasChildNodes() )
	{
    		while ( elm.childNodes.length >= 1 )
    		{
        			elm.removeChild( elm.firstChild );
    		}
	}
}


function loopSelected(selObj, newObj, selectedElm)
{

	for (i=0; i<selObj.options.length; i++) {
		var op = document.createElement('option');
		op.text = selObj.options[i].text;
		op.value = selObj.options[i].value;
		op.id = selObj.options[i].id;
		if (selectedElm == selObj.options[i].value ){
			op.selected = 'selected';
		}
		try {
  			newObj.add(op, newObj.options[i]); // firefox
		} catch (ex) {
  			newObj.add(op, i); // ie
		}
	}
}


function showSelected(selectedOption, id  ) {

		var selOperatorObj = document.getElementById('exlidInput_precisionOperator_' +id);
		var selOperatorIndex = selOperatorObj.selectedIndex;
		var titleObj=document.getElementById('exlidInput_scope_title' +id);

		var allObj =document.getElementById('exlidInput_scope_all' +id);
		var orgObj = document.getElementById('exlidInput_scope_' +id);
		var selectedElm =orgObj.options[orgObj.selectedIndex];

		if ( selOperatorObj.options[selOperatorIndex].value =='begins_with'){
			if (titleObj == null || titleObj == undefined ){
				selectedOption.options[0].selected = 'selected';
				alert('<fmt:message key="default.title.missing.error.search.searchoperator" />');
				return false;
			}
			removeChildren(orgObj);
			loopSelected(titleObj, orgObj, selectedElm.value);
			var searchForm = document.getElementsByName('searchForm')[0];
			var hiddenSrt = document.getElementById("str");
			hiddenSrt.value = 'title';
		} else{
			removeChildren(orgObj);
			loopSelected(allObj, orgObj, selectedElm.value);
			var searchForm = document.getElementsByName('searchForm')[0];
			var hiddenSrt = document.getElementById("str");
			hiddenSrt.value = 'rank';
		}

}

function unselect(id  ) {

	 	 if (document.getElementById('begins_with'+id)) {
			var orgObj = document.getElementById('exlidInput_scope_' +id);
			removeChildren(orgObj);
			var op = document.createElement('option');
			op.text = '<fmt:message key="default.search-simple.scope.option.title"  />';
			op.value = 'title';
			op.id = 'scope_titleOnly'+id;
			op.selected = 'selected';
			try {
  				orgObj.add(op, orgObj.options[0]); // firefox
			} catch (ex) {
  				orgObj.add(op, 0); // ie
			}
			var searchForm = document.getElementsByName('searchForm')[0];
			var hiddenSrt = document.getElementById("str");
			hiddenSrt.value = 'title';
		}
}

function unselectAdvanceTitle(id){

	if (document.getElementById('title'+id)) {
		var orgObj = document.getElementById('exlidInput_scope_' +id);
		removeChildren(orgObj);
		var op = document.createElement('option');
		op.text = '<fmt:message key="default.search-advanced.scope.option.title"  />';
		op.value = 'title';
		op.id = 'scope_titleOnly'+id;
		op.selected = 'selected';
		try {
			orgObj.add(op, orgObj.options[0]); // firefox
		} catch (ex) {
			orgObj.add(op, 0); // ie
		}
		var searchForm = document.getElementsByName('searchForm')[0];
		var hiddenSrt = document.getElementById("str");
		hiddenSrt.value = 'title';
	}
}


//]]>
</script>