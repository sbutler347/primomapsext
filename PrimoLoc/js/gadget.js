var tooltipShown = false;

function show(linx, object) {

	if(!tooltipShown)
	{
		if (document.getElementById)
		{
			leftx = (findPosX(linx))-52;
			topx = (findPosY(linx))+20;
			document.getElementById(object).style.left = leftx+'px';
			document.getElementById(object).style.top = topx+'px';
			document.getElementById(object).style.visibility = 'visible';
			tooltipShown = true;
		}
	}
	else
	{
		tooltipShown = false;
		document.getElementById(object).style.visibility = 'hidden';
	}

}

function findPosX(obj)
{
	var curleft = 0;

	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft;
}

function findPosY(obj)
{
	var curtop = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
	return curtop;
}

function onSearch()
{
	window.resizeTo(601,550);
}



function updateRTA(overrideIsUpdateAllowed){
	//if (isUpdateAllowed(overrideIsUpdateAllowed)){
		var queryStatusURL = "rta.do?isGadget=true";
		ajaxGet(queryStatusURL, handleGadgetRtaResponse);
	//}
}

function handleGadgetRtaResponse(){
	try{
	if(checkReadyState(request)) //check to see if we have data.
	{
			//here's the trick. we receive a statusObj serialized via JSON. the eval turns it back into an object.
			eval(request.responseText);
			//console.log("response:"+request.responseText);
			if (statusObj && statusObj.length > 0){
				for (var i = 0; i<statusObj.length; i++){
				try{
					document.getElementById('delivery_' + i).className = statusObj[i][1];
					document.getElementById('delivery_' + i).title = statusObj[i][0];
					}catch(e){
					
					}
				}
	
			}
			
	}else{//error?
		//alert("else error");
	}
	}catch(e){
		//alert(e);
	}
}
