/*
	Feel free to use your custom icons for the tree. Make sure they are all of the same size.
	User icons collections are welcome, we'll publish them giving all regards.
*/

var TREE_TPL = {
	'target'  : 'frameset',	// name of the frame links will be opened in
							// other possible values are: _blank, _parent, _search, _self and _top

	'icon_e'  : '../icons/tree/empty.gif', // empty image 
	'icon_l'  : '../icons/tree/line.gif',  // vertical line

    'icon_32' : '../icons/tree/base.gif',   // root leaf icon normal
    'icon_36' : '../icons/tree/base.gif',   // root leaf icon selected
	
	'icon_48' : '../icons/tree/base.gif',   // root icon normal
	'icon_52' : '../icons/tree/base.gif',   // root icon selected
	'icon_56' : '../icons/tree/base.gif',   // root icon opened
	'icon_60' : '../icons/tree/base.gif',   // root icon selected
	
	'icon_16' : '../icons/tree/folder.gif', // node icon normal
	'icon_20' : '../icons/tree/folderopen.gif', // node icon selected
	'icon_24' : '../icons/tree/folderopen.gif', // node icon opened
	'icon_28' : '../icons/tree/folderopen.gif', // node icon selected opened

	'icon_0'  : '../icons/tree/page.gif', // leaf icon normal
	'icon_4'  : '../icons/tree/page.gif', // leaf icon selected
	
	'icon_2'  : '../icons/tree/joinbottom.gif', // junction for leaf
	'icon_3'  : '../icons/tree/join.gif',       // junction for last leaf
	'icon_18' : '../icons/tree/plusbottom.gif', // junction for closed node
	'icon_19' : '../icons/tree/plus.gif',       // junctioin for last closed node
	'icon_26' : '../icons/tree/minusbottom.gif',// junction for opened node
	'icon_27' : '../icons/tree/minus.gif'       // junctioin for last opended node
};

