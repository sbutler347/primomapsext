var autoComplete = new Array("Bears", "Berlin", "ofir", "Bear Mountain", "Bears, Grizzly", "Bears, brown", "Chicago Bears" );
var viewx = true;
var captureKey = true;
var rownum = -1;
var tempLink;
var submit_but = new Image; submit_but.src = "../images/go_search.gif"
var submit_but_o = new Image; submit_but_o.src = "../images/go_search_o.gif"


function checkx(obj,text,divz)
{
	var string = "";
	var counter = 0;
	rownum = -1;	
	if(text.length == 1)
	{
		for(i=0;i<autoComplete.length;i++)
		{
			lowerTemp = autoComplete[i].toLowerCase();
			temp = lowerTemp.substr(0,text.length);
			if(temp==text)
			{
				if(viewx)
				{
					show(obj, divz);
					viewx = false;
				}
				string = addToString(autoComplete[i] , string, counter);
				counter++;
			}
			else if(autoComplete[i].indexOf(" ") != -1)
			{
				tempLoc = autoComplete[i].indexOf(" ")+1;
				temp = lowerTemp.substr(tempLoc,text.length);
				if(temp==text)
				{
					if(viewx)
					{
						show(obj, divz);
						viewx = false;
					}
					string = addToString(autoComplete[i] , string, counter);
					counter++;					
				}
			}
		}
	}
	else if(text.length==0)
	{
		 hide(divz);
		 viewx = true;
	}
	else
	{
		for(i=0;i<autoComplete.length;i++)
		{
			lowerTemp = autoComplete[i].toLowerCase();
			temp = lowerTemp.substr(0,text.length);
			if(temp==text)
			{
				if(viewx)
				{
					show(obj, divz);
					viewx = false;
				}				
				string = addToString(autoComplete[i] , string, counter);
				counter++;
			}
			else if(autoComplete[i].indexOf(" ") != -1)
			{
				tempLoc = autoComplete[i].indexOf(" ")+1;
				temp = lowerTemp.substr(tempLoc,text.length);
				if(temp==text)
				{
					if(viewx)
					{
						show(obj, divz);
						viewx = false;
					}
					string = addToString(autoComplete[i] , string, counter);
					counter++;	
				}				
			}
		}
	}
	if(string.length>0) document.getElementById(divz).innerHTML = string;
	else
	{
		hide(divz);
		viewx = true;
	}	
}

function addToString(str , list, num)
{
	list+= "<a href='#"+str+"' onmouseover='colorme("+num+");captureKey=false;' onmouseout='cleanColor(); rownum = -1; captureKey = true;' class='auto'>"+str +"</a>";
	return list;
}

function colorme(whom) 
{
	var Alink_results = [];
	for(a=0;a<(document.getElementsByTagName("A").length);a++)
	{
		if(document.getElementsByTagName("A").item(a).className=="auto")
		{
			obj = document.getElementsByTagName("A").item(a);
			Alink_results[Alink_results.length] = obj;
		}
	}
	
	rownum = whom;
	cleanColor();
	Alink_results[whom].style.backgroundColor = '#DAE8F2';
	Alink_results[whom].style.color = "#F58B21";
	tempLink = Alink_results[whom].href;
}

function cleanColor()
{
	var Alink_results = [];
	for(a=0;a<(document.getElementsByTagName("A").length);a++)
	{
		if(document.getElementsByTagName("A").item(a).className=="auto")
		{
			obj = document.getElementsByTagName("A").item(a);
			Alink_results[Alink_results.length] = obj;
		}
	}

	for (var i=0; i < Alink_results.length; i++)
	{
		Alink_results[i].style.backgroundColor = '#E9F1F7';
		Alink_results[i].style.color = "#1770B0";
		Alink_results[i].blur();
	}	
}


function whichkey(event) 
{
	if(!viewx)
	{
		var Alink_results = [];
		for(a=0;a<(document.getElementsByTagName("A").length);a++)
		{
			if(document.getElementsByTagName("A").item(a).className=="auto") // dotted line
			{
				obj = document.getElementsByTagName("A").item(a);
				Alink_results[Alink_results.length] = obj;
			}
		}
		event = (event) ? event : window.event

		if(browserDetect('msie'))
		{
			if  (event.keyCode == 40&&captureKey) 
			{
				document.getElementById("search").blur();
				rownum ++;
				if (rownum > (Alink_results.length - 1)) rownum --;
				colorme (rownum);
			}
			else if  (event.keyCode == 38&&captureKey) 
			{
				document.getElementById("search").blur();			
				rownum --;
				if (rownum < 0)
				{
					rownum = -1;
 					document.getElementById("search").focus();
				 }
				 else colorme (rownum);
			}
			else if  (event.keyCode == 13)
			{
				window.location = tempLink;
			}			
		}
	}
}


function show(linx, object) 
{
    if (document.getElementById)
	{
		
		if((browserDetect('opera'))||(browserDetect('firefox')))
		{
			leftx = (findPosX(linx));
			topx = (findPosY(linx))+25;

		}
		else if(browserDetect('msie'))
		{
			leftx = (findPosX(linx));
			topx = (findPosY(linx))+5;
		}
		else
		{
			leftx = (findPosX(linx));
			topx = (findPosY(linx))+25;		
		}

        document.getElementById(object).style.left = leftx+'px';
        document.getElementById(object).style.top = topx+'px';
        document.getElementById(object).style.visibility = 'visible';
	}
}

function hide(obj)
{
	document.getElementById(obj).style.visibility = 'hidden';

}

function findPosX(obj)
{
	var curleft = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft;
}

function findPosY(obj)
{
	var curtop = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
	return curtop;
}

function browserDetect(text) {
   str = (navigator.userAgent.toLowerCase()).indexOf(text) + 1;
   data = text;
   return str;
}

function Cloud_list(x)
{
	if(x)
	{
		document.getElementById("pop_cloud").style.display = "none";
		document.getElementById("pop_list").style.display = "block";
		document.getElementById("linkSpan").innerHTML = "<a href='#' onclick='Cloud_list(false); return false;'>Cloud</a> / List";
	}
	else
	{
		document.getElementById("pop_cloud").style.display = "block";
		document.getElementById("pop_list").style.display = "none";
		document.getElementById("linkSpan").innerHTML = "Cloud / <a href='#' onclick='Cloud_list(true); return false;'>List</a>";

	}
}
