/* result_change.js begin */
function select_tr(objName,docId,vid)
{
	select_tr_display(objName);
	var trx = document.getElementsByTagName('tr');
	var tr_results = [];

	for (var i = 0; i < trx.length; i++)
		if (/\bselected\b/.exec(trx[i].className))
			tr_results[tr_results.length] = trx[i];

	/*
	no need for this , done as server side randering
	for (var i = 0; i < tr_results.length; i++)
	{
		var obj = tr_results[i];
		obj.className = 'text1';
	}
	document.getElementById(objName).className = 'selected';
	*/
	//switch full display selection
	var basketPrefix = addSessionId("basket.do");
	document.location.href=basketPrefix+"?fn=display&doc="+docId+"&vid="+vid;
}


function overTR(obj)
{
	if(obj.className =="" || obj.className =="text1")
	{
		obj.className = "overTR";
	}
	else if(obj.className =="selected")
		obj.className = "selected overTR";
}

function offTR(obj)
{
	if(obj.className =="overTR")
	{
		obj.className = "";
	}
	else if(obj.className =="selected overTR")
		obj.className = "selected";
}

function iframeGon()
{try{
	var obj = document.getElementById("demoLib");

	if(obj!=null && (obj.contentWindow.document.body)&&(obj.contentWindow.document.body.scrollHeight>200))
	{
		innerObj = ((obj.contentWindow.document.body.offsetHeight)-20)+"px";
		obj.style.height = innerObj;
	}
	else setTimeout("iframeGon()", 100);
}catch(e){}
}

function browserDetect(text) {
   str = (navigator.userAgent.toLowerCase()).indexOf(text) + 1;
   data = text;
   return str;
}


function select_tr_display(objName)
{
	var trx = document.getElementsByTagName('tr');
	var tr_results = [];

	for (var i = 0; i < trx.length; i++)
		if (/\bselected\b/.exec(trx[i].className))
			tr_results[tr_results.length] = trx[i];

	for (var i = 0; i < tr_results.length; i++)
	{
		var obj = tr_results[i];
		obj.className = 'text1';
	}
	document.getElementById(objName).className = 'selected';
}

function checkAll(bin)
{
	if(!bin)
	{
		var chkbox = document.getElementsByTagName('input');
		for(i=0;i<chkbox.length; i++)
			if((chkbox[i].type=='checkbox')&&(chkbox[i].className='chkbox'))
				chkbox[i].checked=0;
	}
	else
	{
		var chkbox = document.getElementsByTagName('input');
		for(i=0;i<chkbox.length; i++)
			if((chkbox[i].type=='checkbox')&&(chkbox[i].className='chkbox'))
				chkbox[i].checked=1;
	}
}


//---------------------------------------------------------------------------------------------------------------------------------
var firstTime_drag = true;
function setevent()
{
	document.getElementById("draggable").onmousedown = Position;
	document.onmouseup = Aktion;
	document.onmousemove = NeuPos;
}

var startpos, diffpos=47;
var erlaubt = false;

function Position(Ereignis)
{
	if (!document.all) startpos = Ereignis.screenY + diffpos;
	else startpos = event.clientY + diffpos;
	erlaubt = true;
	return false;
}

function Aktion(Ereignis)
{
	erlaubt = false;
}

function NeuPos(Ereignis)
{
	if (erlaubt)
	{
		if (!document.all) jetztpos = Ereignis.screenY;
		else jetztpos = event.clientY;
		diffpos = startpos-jetztpos;
		if (diffpos > -500 && diffpos < 250)
		{
			document.getElementById("itemsList_id").style.height = 200 - diffpos + "px";
			if(document.getElementById("foldersOverflow")) document.getElementById("foldersOverflow").style.height = 229 - diffpos + "px";
		}
	}
}


/***********************************************
* IFrame SSI script II- ? Dynamic Drive DHTML code library (http://www.dynamicdrive.com)
* Visit DynamicDrive.com for hundreds of original DHTML scripts
* This notice must stay intact for legal use
***********************************************/

//Input the IDs of the IFRAMES you wish to dynamically resize to match its content height:
//Separate each ID with a comma. Examples: ["myframe1", "myframe2"] or ["myframe"] or [] for none:
var iframeids=["demoLibId"]

//Should script hide iframe from browsers that don't support this script (non IE5+/NS6+ browsers. Recommended):
var iframehide="yes"

var getFFVersion=navigator.userAgent.substring(navigator.userAgent.indexOf("Firefox")).split("/")[1]
var FFextraHeight=parseFloat(getFFVersion)>=0.1? 16 : 0 //extra height in px to add to iframe in FireFox 1.0+ browsers

function resizeCaller() {
var dyniframe=new Array()
for (i=0; i<iframeids.length; i++){
if (document.getElementById)
resizeIframe(iframeids[i])
//reveal iframe for lower end browsers? (see var above):
if ((document.all || document.getElementById) && iframehide=="no"){
var tempobj=document.all? document.all[iframeids[i]] : document.getElementById(iframeids[i])
tempobj.style.display="block"
}
}
}

function resizeIframe(frameid){
var currentfr=document.getElementById(frameid)
if (currentfr && !window.opera){
currentfr.style.display="block"
if (currentfr.contentDocument && currentfr.contentDocument.body.offsetHeight) //ns6 syntax
currentfr.height = currentfr.contentDocument.body.offsetHeight+FFextraHeight;
else if (currentfr.Document && currentfr.Document.body.scrollHeight) //ie5+ syntax
currentfr.height = currentfr.Document.body.scrollHeight;
if (currentfr.addEventListener)
currentfr.addEventListener("load", readjustIframe, false)
else if (currentfr.attachEvent){
currentfr.detachEvent("onload", readjustIframe) // Bug fix line
currentfr.attachEvent("onload", readjustIframe)
}
}
}

function readjustIframe(loadevt) {
var crossevt=(window.event)? event : loadevt
var iframeroot=(crossevt.currentTarget)? crossevt.currentTarget : crossevt.srcElement
if (iframeroot)
resizeIframe(iframeroot.id);
}

function loadintoIframe(iframeid, url){
if (document.getElementById)
document.getElementById(iframeid).src=url
}

if (window.addEventListener)
window.addEventListener("load", resizeCaller, false)
else if (window.attachEvent)
window.attachEvent("onload", resizeCaller)
else
window.onload=resizeCaller

/* result_change.js end */
