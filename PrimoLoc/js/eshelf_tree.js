
function initTree()
{
	hideChecks();
	closeAll_branches();
}

function hideChecks()
{
	var inputArr = document.getElementById('foldersOverflow').getElementsByTagName('input');
	for(i=0; i<inputArr.length; i++)
		if(inputArr[i].type=='checkbox') inputArr[i].style.display = 'none';
}

function toggleBranch(linkBranch,folderId)
{

	var isOpen
	if((linkBranch.src).indexOf("minus") != -1)
	{

		linkBranch.parentNode.parentNode.getElementsByTagName('ul')[0].style.display = "none";
		linkBranch.src = "../images/tree_plus.gif";
		isOpen= 'false';
		ajaxGet("basket.do"+"?fn=collapse"+"&folderId="+folderId+"&exemode=async&isOpen="+isOpen, emptyFunction);
	}
	else if((linkBranch.src).indexOf("plus") != -1)
	{

		linkBranch.parentNode.parentNode.getElementsByTagName('ul')[0].style.display = "";
		linkBranch.src = "../images/tree_minus.gif";
		isOpen= 'true';
		ajaxGet("basket.do"+"?fn=collapse"+"&folderId="+folderId+"&exemode=async&isOpen="+isOpen, emptyFunction);
	}
}

function closeAll_branches()
{
	var imgArr = document.getElementById('foldersOverflow').getElementsByTagName('img');
	for(i=0; i<imgArr.length; i++)
		toggleBranch(imgArr[i]);
}

/* note bubbles */

function toggle_bubbleNote(linx, note_objDiv)
{
	layerArr = document.getElementsByTagName('div');
	for(i=0; i< layerArr.length; i++)
		if(layerArr[i].className == 'noteBubble')
			layerArr[i].style.visibility = 'hidden';

	if(bubbleAvail_timer) clearTimeout(bubbleAvail_timer);
	document.getElementById(note_objDiv).style.display = "";

	if((browserDetect('opera'))||(browserDetect('firefox')))
	{
		leftx = (findPosX(linx))-264;
		topx = (findPosY(linx))+12;
	}
	else if(browserDetect('msie'))
	{
		leftx = (findPosX(linx))-264;
		topx = (findPosY(linx))+12;
	}
	else
	{
		leftx = (findPosX(linx))-264;
		topx = (findPosY(linx))+12;
	}

	if(leftx<0) leftx = 0;
	document.getElementById(note_objDiv).style.left = leftx+'px';
	document.getElementById(note_objDiv).style.top = topx+'px';
	document.getElementById(note_objDiv).style.visibility = 'visible';
}

function toggleEdit(note_objDiv)
{
	//toggle_bubbleNote
	document.getElementById(note_objDiv).style.visibility = 'visible';
}
