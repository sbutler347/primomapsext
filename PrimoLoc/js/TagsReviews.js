<script type="text/javascript">

	function addIEQuotes(el, text, which){
		if (which == 1 ){
			alert("HI");
		}
		
	}
	function cancel(id)
	{
		if(confirm("<fmt:message key='default.reviews.cancelReviewConfirm'/>"))
		{
			var parentWrite = $('#tagsReviews'+id).find('.EXLReviewsContent');
			//var writeForm = $(parentWrite).find('#writeRev'+id).get(0);
			//$(writeForm).css("display","block");
			$(parentWrite).css("display","block");
			var tagParent = $('#tagsReviews'+id).find('.EXLTagsContainer');
			$(tagParent ).css("display","block");
			var revForm = $('#tagsReviews'+id).find('#review_form'+id).get(0);
			$(revForm).css("display","none");
			// reset values
			$('#tagsReviews'+id).find('#review_text'+id).val('');
			$('#tagsReviews'+id).find('#rateReview'+id).val('1');
			$('#tagsReviews'+id).find('#displayReviewWriter').attr('checked', false);
			$('#tagsReviews'+id).find('#agree04'+id).attr('checked', false);
			$('#tagsReviews'+id).find('#executeMode').val('0');
		}
	}


	function show_more(){
	}

	function prepare_edit(id, rateReview,displayReviewWriter,reviewId)
	{
		if (!loginCheck(id) || !userCheck(id, reviewId)) {
			return;
		}
		
		var parentWrite = $('#tagsReviews'+id).find('.EXLReviewsContent');
		//var writeForm = $(parentWrite).find('#writeRev'+id).get(0);
		//$(writeForm).css("display","none");
		$(parentWrite).css("display","none");
		var tagParent = $('#tagsReviews'+id).find('.EXLTagsContainer');
		$(tagParent ).css("display","none");
		var revForm = $('#review_form'+id).get(0);
		$(revForm).css("display","block");
		if($('#full_review_text_'+reviewId).length > 0){
			var text= $('#tagsReviews'+id).find('#full_review_text_'+reviewId).text();
		}else{
			var text= $('#tagsReviews'+id).find('#ext_val_more_'+reviewId).text();
		}
		var isIE = navigator.userAgent.indexOf("MSIE") != -1;
			if (isIE){
				// IE does not remove quotes
				var result = text.substring(1, text.length - 1);
    			$('#tagsReviews'+id).find('#review_text'+id).val(result);				
			} else{
				$('#tagsReviews'+id).find('#review_text'+id).val(text);
			}
		$('#tagsReviews'+id).find('#rateReview'+id).val(rateReview);
		$('#tagsReviews'+id).find('#reviewId').val(reviewId);

		if (displayReviewWriter == 'true') {
			$('#tagsReviews'+id).find('#displayReviewWriter').attr('checked', true);
		} else{
			$('#tagsReviews'+id).find('#displayReviewWriter').attr('checked', false);
		}

		$('#tagsReviews'+id).find('#agree04'+id).attr('checked', false);
		$('#tagsReviews'+id).find('#executeMode').val('2');
	}


	function edit_save(id)
	{
		if (!loginCheck(id)) {
			return;
		}

		if($('#tagsReviews'+id).find('#agree04'+id).is(':checked'))
		{
			var text = $('#tagsReviews'+id).find('#review_text'+id).val();
			$('#tagsReviews'+id).find('#review_text'+id).val(text);
			var parentWrite = $('#tagsReviews'+id).find('.EXLReviewsContent');
			//var writeForm = $(parentWrite).find('#writeRev'+id).get(0);
			//$(writeForm).css("display","block");
			$(parentWrite).css("display","block");
			var tagParent = $('#tagsReviews'+id).find('.EXLTagsContainer');
			$(tagParent ).css("display","block");
			var revForm = $('#review_form'+id).get(0);
			$(revForm).css("display","none");
	      		$('#tagsReviews'+id).submit();
		}
		else alert("<fmt:message key='default.reviews.agreeError'/>");
		return false;
	}



	function remove(id, reviewId)
	{
		if (!loginCheck(id) || !userCheck(id, reviewId)) {
			return;
		}

		if(confirm("<fmt:message key='default.reviews.cancelReviewConfirm'/>"))
		{
			var parentWrite = $('#tagsReviews'+id).find('.EXLReviewsContent');
			//var writeForm = $(parentWrite).find('#writeRev'+id).get(0);
			//$(writeForm).css("display","block");
			$(parentWrite).css("display","block");
			var tagParent = $('#tagsReviews'+id).find('.EXLTagsContainer');
			$(tagParent ).css("display","block");
			$('#tagsReviews'+id).find('#executeMode').val('3');
			$('#tagsReviews'+id).find('#reviewId').val(reviewId);
			$('#tagsReviews'+id).submit();
		}

	}

	function new_review(id)
	{
		if (!loginCheck(id)) {
			return;
		}
		
		var parentWrite = $('#tagsReviews'+id).find('.EXLReviewsContent');
		//var writeForm = $(parentWrite).find('#writeRev'+id).get(0);
		//$(writeForm).css("display","none");
		$(parentWrite).css("display","none");
		var tagParent = $('#tagsReviews'+id).find('.EXLTagsContainer');
		$(tagParent ).css("display","none");
		var revForm = $('#tagsReviews'+id).find('#review_form'+id).get(0);
		$(revForm).css("display","block");
		$('#tagsReviews'+id).find('#review_text'+id).val('');
		$('#tagsReviews'+id).find('#executeMode').val('1');
		$('#tagsReviews'+id).find('#rateReview'+id).val('1');
		$('#tagsReviews'+id).find('#displayReviewWriter').attr('checked', false);
		$('#tagsReviews'+id).find('#agree04'+id).attr('checked', false);

	}


	function showMore(extValueId){
		var localParent = $(this).parent('.EXLReviewsContent').get(0);
		var extVal = $(localParent).find('#ext_val_'+extValueId).get(0);
		var extValMore = $(localParent).find('#ext_val_more_'+extValueId).get(0);
		var more = $(localParent).find('#more_'+extValueId).get(0);
		$(extVal).css("display","none");
		$(more).css("display","none");
		$(extValMore).css("display","block");
		var isIE = navigator.userAgent.indexOf("MSIE") != -1;
		if (isIE){																		
			var text = extValMore.innerText;						
			extValMore.innerHTML = '&quot;' + text + '&quot;'
		}  
	}



function Tags_Cloud_list(option, which, id)
{
	if(which==2)
	{
		if(option)
		{
			//in an onclick handler, 'this' is the element that received the click.
    			var localParent = $(this).parent('.EXLTagsContainer').get(0);
     			var tagsLinks = $(localParent).find('#everyTags' + id).get(0);
     			var tagsCloud = $(localParent).find('#everyCloudDiv' + id).get(0);
    			 $(tagsCloud).css("display","none");
			$(tagsLinks).css("display","block");
		}
		else
		{
			var localParent = $(this).parent('.EXLTagsContainer').get(0);
			var tagsLinks = $(localParent).find('#everyTags'+id).get(0);
			var tagsCloud = $(localParent).find('#everyCloudDiv'+id).get(0);
			$(tagsCloud).css("display","block");
			$(tagsLinks).css("display","none");
		}
	}

$('.EXLTagsLinksClosed').live('click',function(e){
	  	//notify the browser we handled this click.
		 e.preventDefault();
		 var parent = $(this).parents('.EXLTagsLinks');

		 if($(parent).hasClass('EXLTagsLinksExpand')){
		 	$(parent).removeClass('EXLTagsLinksExpand');
		 }
		 //if tag list is current closed
		 if($(parent).hasClass('EXLTagsLinksClosed')){
		 	var imgElmt = $(this).find('img').get(0);
		  	//change the image to a minus, so that the user can close the list again
      		  	$(imgElmt).attr("src", "<fmt:message key='default.ui.images.tagsreviews.minus'/>");
		  	// and open the list.
		  	$(parent).removeClass('EXLTagsLinksClosed');
		  	// important don't change back
		 	exit();
		 }else{//the tag list is currently open
		 	var imgElmt = $(this).find('img').get(0);
		  	//change image to a plus,
		   	$(imgElmt).attr("src", "<fmt:message key='default.ui.images.tagsreviews.plus'/>");
		   	//close the list.
		  	$(parent).addClass('EXLTagsLinksClosed');
		   	// important don't change back
		  	exit();
		 }

	});
}

//check if user is logged in and if not display login message
function loginCheck(id) {
	if (!isUserLoggedIn()) {
		var content = $('#tagsReviews'+id).find('.EXLReviewsContent');
		$(content).html('<div id="exlidResult${resultStatus.index}-TabHeader" class="EXLTabHeader"><div class="EXLTabHeaderContent"> </div><div id="exlidTabHeaderButtons'+id+'" class="EXLTabHeaderButtons"></div></div><div id="exlidResult${resultStatus.index}-TabContent" class="EXLTabContent"><div id="exlFeedback'+id+'" class="EXLSystemFeedback"><span>' + exlTabSignInMessage + '</span></div></div>');
		return false;
	}
	return true;
}

//check if the user performing the action owns the record performing the action on
//if not display an unautohrised message
function userCheck(id, reviewId) {
    var loggedUser = getUserId();    
    var reviewedUser = $('#reviewUserId_'+reviewId).val();
	if (loggedUser != reviewedUser) {
		var content = $('#tagsReviews'+id).find('.EXLReviewsContent');
		$(content).html('<div id="exlidResult${resultStatus.index}-TabHeader" class="EXLTabHeader"><div class="EXLTabHeaderContent"> </div><div id="exlidTabHeaderButtons'+id+'" class="EXLTabHeaderButtons"></div></div><div id="exlidResult${resultStatus.index}-TabContent" class="EXLTabContent"><div id="exlFeedback'+id+'" class="EXLSystemFeedback"><span>No Authorisation</span></div></div>');
		return false;
	}
	return true;
}
</script>
